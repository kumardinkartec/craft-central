var gulp = require('gulp');
var sass = require('gulp-sass');
var minifyCss = require('gulp-minify-css');

gulp.task('sass', function(done) {
 gulp.src('scss/*.scss')
   .pipe(sass())
   .on('error', sass.logError)
   .pipe(gulp.dest('css/'))
   .on('end', done);
});

gulp.task('watch', function() {
 gulp.watch('scss/*.scss', ['sass']);
});